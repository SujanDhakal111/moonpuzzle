package com.task.moonPuzzle;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SystemInput {
    Integer boundaryX;
    Integer boundaryY;
    public List<RobotInput> robotInputs = new ArrayList<>();

    //To spilit input string according to new lines and add to robotInputs
    public SystemInput(String input){

        String[] splittedInput = input.split("\\r?\\n");
        boundaryX= Integer.parseInt(splittedInput[0].split(" ")[0]);
        boundaryY= Integer.parseInt(splittedInput[0].split(" ")[1]);
        for (int i =1; i <= (splittedInput.length - 1); i+=2){

            robotInputs.add(new RobotInput(splittedInput[i], splittedInput[i+1]));

        }

    }

}
