package com.task.moonPuzzle;

public class RobotInput {
    public int initialX;
    public int initialY;
    public String direction;
    public String commandString;

    //to feed initial position, directon and command
    public RobotInput(String positionString, String movementString) {
        commandString = movementString;
        initialX = Integer.parseInt(positionString.split(" ")[0]);
        initialY = Integer.parseInt(positionString.split(" ")[1]);
        direction = positionString.split(" ")[2];


    }

}
