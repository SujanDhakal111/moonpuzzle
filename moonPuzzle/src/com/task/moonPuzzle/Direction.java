package com.task.moonPuzzle;

public class Direction {
     String getDirectionAfterRotation(String curDir, String rotationDir){
        if(rotationDir.equals("L")){
            return getLeftDirection(curDir);
        } else if (rotationDir.equals("R")){
            return getRightDirection(curDir);
        } else return curDir;
    }

     String getLeftDirection(String curDir) {
        if (curDir.equals("N"))
            return "W";
        else if (curDir.equals("E"))
            return "N";
        else if (curDir.equals("S"))
            return "E";
        else return "S";
    }

     String getRightDirection(String curDir) {

        if (curDir.equals("N"))
            return "E";
        else if (curDir.equals("E"))
            return "S";
        else if (curDir.equals("S"))
            return "W";
        else return "N";
    }

    public Direction(){

    }
}
