package com.task.moonPuzzle;


public class Robot {
    Direction direction1 = new Direction();
    Integer positionX;
    Integer positionY;
    String direction;

    public Robot(){

    }
    //To set initial position of robots according to input
    public void setInitialPositionWithDirection(int initialX, int initialY, String initialDir) {
        positionX = initialX;
        positionY = initialY;
        direction = initialDir;
    }
    //function to make robot movements according to command strings
    public void move(String command){
        int l = command.length();
        for (int i = 0; i < l; i++) {

            if ((command.charAt(i) + "").equals("M")) {
                if (direction.equals("W")) {
                    positionX--;
                } else if (direction.equals("N")) {
                    positionY++;
                } else if (direction.equals("E")) {
                    positionX++;
                } else if (direction.equals("S")) {
                    positionY--;
                }
            } else
                direction = direction1.getDirectionAfterRotation(direction, command.charAt(i) + "");

        }

    }
    //function to get final position of robots
    public String getFinalPosition() {
        System.out.println(+positionX+" " +positionY+" "+direction);
        return (+positionX+" " +positionY+" "+direction);
    }

}
