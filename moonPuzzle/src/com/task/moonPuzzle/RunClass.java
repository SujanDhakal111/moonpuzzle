package com.task.moonPuzzle;


public class RunClass {
    public RunClass(){

    }
    static String input = "6 6" +'\n'+
            "1 2 N" +'\n'+
            "LMLMLMLMM" +'\n'+
            "3 3 E" +'\n'+
            "MMRMMRMRRM";

    //function to perform operations on robot
    static void start(String inputString){
        SystemInput systemInput = new SystemInput(inputString);
        systemInput.robotInputs.forEach(rInput ->{
            Robot robot = new Robot();
            robot.setInitialPositionWithDirection(rInput.initialX, rInput.initialY, rInput.direction);
            robot.move(rInput.commandString);
            robot.getFinalPosition();
        });
    }
    //main function
    public static void main(String[] args) throws InterruptedException {
        start(input);

    }
}


