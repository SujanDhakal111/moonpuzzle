import com.task.moonPuzzle.Robot;
import com.task.moonPuzzle.RunClass;
import com.task.moonPuzzle.SystemInput;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class ResultTest{
    static Robot robot;
    static SystemInput systemInput;
    static RunClass runClass;
    static String input = "6 6" +'\n'+
            "1 2 N" +'\n'+
            "LMLMLMLMM" +'\n'+
            "3 3 E" +'\n'+
            "MMRMMRMRRM";
    static List<String> result = new ArrayList<>();

    @BeforeAll
    static void setRobot(){
        systemInput = new SystemInput(input);
        systemInput.robotInputs.forEach(rInput ->{
            Robot robot = new Robot();
            robot.setInitialPositionWithDirection(rInput.initialX, rInput.initialY, rInput.direction);
            robot.move(rInput.commandString);
            result.add( robot.getFinalPosition());
        });
    }
    @Test
    public void checkResult(){
            Assertions.assertEquals(("[1 3 N, 5 1 E]"), result.toString());

    }



}
